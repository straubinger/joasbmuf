# Prefijos validos y sus usos

Los siguientes prefijos están basados en la Convención de Angular con sus equivalentes al español.

1. `caract` (característica) - para nuevos desarrollos o mejoras en un modulo o librería.
1. `correc` (corrección) - para correcciones de errores o problemas en el código.
1. `docs` (documentación) - para cambios relacionados con la documentación del proyecto.
1. `estilo` (estilo) - para cambios de estilo del código que no afectan la funcionalidad, como indentación, espaciado, etc.
1. `refactor` (refactorización) - para cambios en el código que no corrigen errores ni añaden características, pero mejoran la estructura o legibilidad del código.
1. `prueba` (prueba) - para cambios relacionados con pruebas, como la adición de nuevos casos de prueba o la modificación de pruebas existentes.
1. `tarea` (tarea) - para cambios que no están relacionados directamente con la funcionalidad del código, como la actualización de dependencias, cambios en la configuración del proyecto, etc.

Ejemplos de usos:

```text
caract(auth): añadir funcionalidad de inicio de sesión
correc(ui): corregir alineación de botones
docs(api): actualizar documentación de la API
```

Si el titulo del commit no usa un sufijo acompañado de un modulo, librería o faceta entre paréntesis Git no permitirá que se realice el commit.
