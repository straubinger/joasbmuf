# Joas B. Muf: Jorge Owns a Book Markup Format

Joas B. Muf o *Jorge Owns a Book Markup Format* (Jorge es dueño de un formato de marcado para libros) es un formato de marcado sencillo especialmente pensado para biblias cristianas que sirve para exportar a otros formatos de marcado como Markdown, AsciiDoc, etc. o formatos de archivo como JSON, XML. La intención es facilitar el trabajo de procesamiento de la [Biblia traducida por monseñor Juan Straubinger desde los textos originales](https://gitlab.com/straubinger/biblia).

## Formato propuesto

Sin mucho pensar, este es potencialmente el formato de marcado que rige Joas B. Muf.

## `{v}` indica un versículo

El conteo es automático y se reinicia por cada capitulo. Ejemplo:

``` text
{v}Dixit ergo eis iterum: Pax vobis. Sicut misit me Pater, et ego mitto vos. {v} Haec cum dixisset, insufflavit, et dixit eis: Accipite Spiritum Sanctum: {v}quorum remiseritis peccata, remittuntur eis: et quorum retinueritis, retenta sunt. 
```

Produciría el siguiente resultado en texto plano:

``` text
²¹Dixit ergo eis iterum: Pax vobis. Sicut misit me Pater, et ego mitto vos. ²²Haec cum dixisset, insufflavit, et dixit eis: Accipite Spiritum Sanctum: ²³quorum remiseritis peccata, remittuntur eis: et quorum retinueritis, retenta sunt.
```

`{v}` siempre será colocado junto al lado del siguiente versículo eliminando cualquier espacio en blanco que haya en medio.

## `{0}` agrega un numero al inicio de un párrafo

El conteo no se reinicia, a menos que opcionalmente indique que sea reiniciado entre libros o capítulo. Ejemplo: 

``` text
{0}¿Qué se entiende por la palabra SACRAMENTO? - Por la palabra Sacramento se entiende un signo sensible y eficaz de la gracia, instituido por Jesucristo para santificar nuestras almas.

{0} ¿Por qué llamáis a los sacramentos señales sensibles y eficaces de la gracia? - Llamo a los sacramentos señales sensibles y eficaces de la gracia, porque todos los sacramentos significan, por medio de cosas sensibles, la gracia divina que producen en nuestras almas.

{0}¿Explicadme con un ejemplo cómo los sacramentos son señales sensibles y eficaces de la gracia? - En el Bautismo, el derramar el agua sobre la cabeza del niño y las palabras: Yo te bautizo, esto es, yo te lavo, en el nombre del Padre y del Hijo y del Espíritu Santo, son una señal sensible de los que el Bautismo obra en el alma, porque así como el agua lava el cuerpo, así también la gracia divina del Bautismo limpia de pecado el alma.

{0} ¿Cuántos y cuáles son los sacramentos? - Los sacramentos son siete: Bautismo, Confirmación o Santo Crisma, Eucaristía, Penitencia, Extremaunción, Orden Sacerdotal y Matrimonio.
```

Produciría el siguiente resultado, el separador puede ser configurado opcionalmente y siempre añade un espacio en blanco entre si mismo y el texto posterior:

``` text
519.- ¿Qué se entiende por la palabra SACRAMENTO? - Por la palabra Sacramento se entiende un signo sensible y eficaz de la gracia, instituido por Jesucristo para santificar nuestras almas.

520.- ¿Por qué llamáis a los sacramentos señales sensibles y eficaces de la gracia? - Llamo a los sacramentos señales sensibles y eficaces de la gracia, porque todos los sacramentos significan, por medio de cosas sensibles, la gracia divina que producen en nuestras almas.

521.- ¿Explicadme con un ejemplo cómo los sacramentos son señales sensibles y eficaces de la gracia? - En el Bautismo, el derramar el agua sobre la cabeza del niño y las palabras: Yo te bautizo, esto es, yo te lavo, en el nombre del Padre y del Hijo y del Espíritu Santo, son una señal sensible de los que el Bautismo obra en el alma, porque así como el agua lava el cuerpo, así también la gracia divina del Bautismo limpia de pecado el alma.

522.- ¿Cuántos y cuáles son los sacramentos? - Los sacramentos son siete: Bautismo, Confirmación o Santo Crisma, Eucaristía, Penitencia, Extremaunción, Orden Sacerdotal y Matrimonio.
```

¡Usarlo como `{v}` entre un conjunto de palabras producirá un error!

## {m} para «notas al pie» o «notas al final»

Si hubiese necesidad de agregar notas al pie (aparecen al final de la pagina) o notas al final (aparecen al final del capitulo o del libro), se puede utilizar `{m}`, por defecto la información contenida se agregara como notas al pie de pagina e insertará un `*` para demarcar al lector que hay una nota al final de pagina. Si la nota estaba entre 2 versículos esto se inserta como parte de la nota. Toda nota es finalizada con un punto y final (.), ejemplo:

``` text
{v} He aquí que ya clama el jornal sustraído por vosotros a los trabajadores que segaron vuestros campos, y el clamor de los segadores ha penetrado en los oídos del Señor de los ejércitos {m: Véase {l:ef 6, 5-} y nota}.
```

produciría el siguiente resultado, en texto plano:

``` text
⁴He aquí que ya clama el jornal sustraído por vosotros a los trabajadores que segaron vuestros campos, y el clamor de los segadores ha penetrado en los oídos del Señor de los ejércitos*.

// (luego al final de la pagina...)

4. Véase Ef. 6, 5 ss. y nota.
```

## `{l}` para insertar referencias a otros lugares

tal y como se vio en la explicación para `{m}`, `{l}` funciona para insertar referencias a otros lugares. En texto plano esto no tendrá otro efecto que insertar texto, pero en documentos como HTML creara enlaces a hacia esos sitios al que el lector podrá navegar.

Para insertar una referencia a algún lugar de la Biblia, se puede usar la notación usual como en el ejemplo anterior `{l:ef 6, 5-}` inserta `Ef. 6, 5 ss.`, escribir `{l:ef 6}` inserta `Ef. 6`, `{l:ef}` y `{l:ef 1}` insertan lo mismo, `Ef. 1`.

Para hacer uso de este modo de referencia, es necesario registrar cada libro indicando su acrónimo y su nombre corto. Así, si registramos "La Carta de San Pablo a los Efesios" con el nombre corto `efesios` y el acrónimo `ef` podremos usarlo en otros lugares. Insertar una referencia a un libro que no esta registrado produce una advertencia e inserta el contenido como tal, por ejemplo `{l:Austin 3:16}` inserta `Austin 3:16` y en documentos como HTML no produce un enlace.

Para referencia de algún capitulo o versículo dentro del mismo libro basta con omitir el «acrónimo» o el «nombre corto», así si dentro de "La Carta de San Pablo a los Efesios" `{l:6, 5}` inserta `Ef. 6, 5`, `{l:,5}` inserta `v. 5` (versículo 5) y `{l:6}` inserta `Ef. 6` (Efesios, capitulo 6). Naturalmente todas estas inserciones marcan el texto como un enlace en documentos como HTML.

Si quiere insertar referencias a párrafo enumerados con `{0}`, necesita marcar la etiqueta con un identificador único que luego puede ser usado dentro de una etiqueta de referencia, por ejemplo:

``` text
{0}¿Qué se entiende por la palabra SACRAMENTO? - Por la palabra Sacramento se entiende un signo sensible y eficaz de la gracia, instituido por Jesucristo para santificar nuestras almas.

{0:hola} ¿Por qué llamáis a los sacramentos señales sensibles y eficaces de la gracia? - Llamo a los sacramentos señales sensibles y eficaces de la gracia, porque todos los sacramentos significan, por medio de cosas sensibles, la gracia divina que producen en nuestras almas.

{0}¿Explicadme con un ejemplo cómo los sacramentos son señales sensibles y eficaces de la gracia? - En el Bautismo, el derramar el agua sobre la cabeza del niño y las palabras: Yo te bautizo, esto es, yo te lavo, en el nombre del Padre y del Hijo y del Espíritu Santo, son una señal sensible de los que el Bautismo obra en el alma, porque así como el agua lava el cuerpo, así también la gracia divina del Bautismo limpia de pecado el alma.

{0} ¿Cuántos y cuáles son los sacramentos? - Los sacramentos son siete: Bautismo, Confirmación o Santo Crisma, Eucaristía, Penitencia, Extremaunción, Orden Sacerdotal y Matrimonio.

// (más adelante en otro lugar del libro...)

... para conocer más, mire la pregunta n.º {l:{0:hola}}.
```

Producirá lo siguiente:

``` text
519.- ¿Qué se entiende por la palabra SACRAMENTO? - Por la palabra Sacramento se entiende un signo sensible y eficaz de la gracia, instituido por Jesucristo para santificar nuestras almas.

520.- ¿Por qué llamáis a los sacramentos señales sensibles y eficaces de la gracia? - Llamo a los sacramentos señales sensibles y eficaces de la gracia, porque todos los sacramentos significan, por medio de cosas sensibles, la gracia divina que producen en nuestras almas.

521.- ¿Explicadme con un ejemplo cómo los sacramentos son señales sensibles y eficaces de la gracia? - En el Bautismo, el derramar el agua sobre la cabeza del niño y las palabras: Yo te bautizo, esto es, yo te lavo, en el nombre del Padre y del Hijo y del Espíritu Santo, son una señal sensible de los que el Bautismo obra en el alma, porque así como el agua lava el cuerpo, así también la gracia divina del Bautismo limpia de pecado el alma.

522.- ¿Cuántos y cuáles son los sacramentos? - Los sacramentos son siete: Bautismo, Confirmación o Santo Crisma, Eucaristía, Penitencia, Extremaunción, Orden Sacerdotal y Matrimonio.

// ...

... para conocer más, mire la pregunta n.º 520.
```

Las etiquetas `{0}` con identificadores repetidos (insensibles a mayúscula, por lo que `hola` y `Hola` son el mismo identificador), sin usar, o cuyo identificador inicie con o sea solamente un carácter invalido produce un error. 

## Formato de texto

### {b} para texto en Negrita

Por ejemplo `al nacer dije {b: hola mundo}.` produciría en tipos de documento donde tenga sentido este demarcado, como por en documentos HTML, lo siguiente:

``` html
al nacer dije <strong>hola mundo</strong>
```

y en el navegador debería de verse así:

> al nacer dije **hola mundo**

### {k} para texto en cursivas

Por ejemplo `al nacer dije {k: hola mundo}.` produciría en tipos de documento donde tenga sentido este demarcado, como por en documentos HTML, lo siguiente:

``` html
al nacer dije <emph>hola mundo</emph>
```

y en el navegador debería de verse así:

> al nacer dije *hola mundo*

### {s} para texto subrayado

Por ejemplo `al nacer dije {s: hola mundo}.` produciría en tipos de documento donde tenga sentido este demarcado, como por en documentos HTML, lo siguiente:

``` html
al nacer dije <u>hola mundo</u>
```

y en el navegador debería de verse así:

> al nacer dije <u>hola mundo</u>

### {t} para texto tachado

Por ejemplo `al nacer dije {t: hola mundo}.` produciría en tipos de documento donde tenga sentido este demarcado, como por en documentos HTML, lo siguiente:

``` html
al nacer dije <del>hola mundo</del>
```

y en el navegador debería de verse así:

> al nacer dije <del>hola mundo</del>

### {^} para super indices

Por ejemplo `al nacer dije {^: hola mundo}.` produciría en tipos de documento donde tenga sentido este demarcado, como por en documentos HTML, lo siguiente:

``` html
al nacer dije <sup>hola mundo</sup>
```

y en el navegador debería de verse así:

> al nacer dije <sup>hola mundo</sup>

en texto plano, por ejemplo, no tendrá ningún efecto.

### {~} para sub-indices

Por ejemplo `al nacer dije {~: hola mundo}.` produciría en tipos de documento donde tenga sentido este demarcado, como por en documentos HTML, lo siguiente:

``` html
al nacer dije <sub>hola mundo</sub>
```

y en el navegador debería de verse así:

> al nacer dije <sub>hola mundo</sub>

en texto plano, por ejemplo, no tendrá ningún efecto.

### {!} para versalitas

Por ejemplo `entonces le dijo a {!:Dios}:` produciría en tipos de documentos donde tenga sentido este demarcado lo siguiente:

Texto plano:

``` text
entonces le dijo a DIOS:
```

En HTML:

``` html
entonces le dijo a <span style="font-variant: small-caps;">Dios</span>:
```

### Usar varios formatos a la vez

Para marcar un texto con Negrita y Cursiva puede usar el nombre de las etiquetas de cada uno en una sola etiqueta, por ejemplo `{nk: hola mundo}` produciría lo siguiente en documentos como HTML:

``` html
<strong><emph>hola mundo</emph></strong>
```

y en el navegador debería verse así:

> **_hola mundo_**

Entonces, puede usar `n`, `k`, `s`, `t`, `^` y `~` juntos sin violentar ninguna regla del formato de marcado.

### Capítulo, secciones y sub-secciones

Para la división lógica de un libro se utiliza capítulo, secciones y sub-secciones. Para demarcar estas divisiones lógicas se usará números romanos, por tanto un capitulo sería `{i}`, una sección `{ii}` y más sub-secciones hasta los 12 niveles sería `{xii}`. Para explicar el nombre de alguno de estos elementos, se escribe como argumento, por ejemplo:

``` text
{i:Prologo} {!:En el cual se trata de cuan necesarios son los pastores en la Iglesia, de su autoridad y oficio, y de las partes principales de la doctrina cristiana.}
```

Producirá lo siguiente:

``` text
Prologo

EN EL CUAL SE TRATA DE CUAN NECESARIOS SON LOS PASTORES EN LA IGLESIA, DE SU AUTORIDAD Y OFICIO, Y DE LAS PARTES PRINCIPALES DE LA DOCTRINA CRISTIANA.
```

A pesar que Prologo y EN EL CUAL están separados como párrafos, **son una misma unidad**. Por lo que en un índice aparecerían juntos separados por una coma en lugar de dos saltos de lineas:

``` text
Prologo, en el cual se trata de cuan necesarios son los pastores en la Iglesia, de su autoridad y oficio, y de las partes principales de la doctrina cristiana.
```

Cuando se desea mostrar el numero del capitulo, puede usarse `*` como argumento, por ejemplo `{i *}` producirá `1`, y para usar nombres más largos `{i **}` producirá `Capitulo 1`, en un ejemplo más elaborado:

``` text
{i **} {!:Del primer artículo del credo.}
{ii} Creo en Dios Padre Todopoderoso, Criador del cielo y de la tierra.
```

Produciría lo siguiente:

``` text
Capitulo 2

DEL PRIMER ARTÍCULO DEL CREDO.

Creo en Dios Padre Todopoderoso, Criador del cielo y de la tierra.
```
#### Partes

Para dividir lógicamente por partes, lo cual es una unidad por encima de *Capitulo* en la jerarquía, se puede usar el numero romano en mayúscula `{I}`.

## {initium} y {finis}

Bajo algunas circunstancias se requiere envolver fragmentos de texto para otorgarles un formato especial, la envoltura estará delimitada por los términos clave `{initium}` y `{finis}`, palabras latinas para "inicio" y "fin".

### Textos bilingües

Para crear textos bilingües se usará `bilinguis` (sí, otra palabra latina 🤷), por ejemplo:

``` text
{initium:bilinguis}
  {dexter}
    {ii} Homo non potest, suis relictus viribus, veram sapientiam, & obtinende beatitudinis certas rationes consequi.
  {sinister}
    {ii} No puede el hombre por solas sus fuerzas alcanzar la verdadera sabiduría, ni los medios necesarios para su salvacion.
{finis:bilinguis}
```

Producirá lo siguiente (al menos en texto plano):

``` text
1 Homo non potest, suis relictus   1 No puede el hombre por solas sus
viribus, veram sapientiam, &       fuerzas alcanzar la verdadera
obtinende beatitudinis certas      sabiduría, ni los medios
rationes consequi.                 necesarios para su salvacion.
```

Dejar `{dexter}` y/o `{sinister}` en blanco producirá un error. Usar cualquiera de estas palabras clave fuera del bloque `{initium:bilinguis}` `{finis:bilinguis}` producirá un error.

### Texto en prosa
Estos tipos de textos son respetados tal cual son insertados. La palabra clave sería `prosaica`, ejemplo:

``` text
Último discurso de Job. {v}Siguió explicando y dijo:

{initium:prosaica}
  {v}"¡Ojalá volviera a ser 
  como en los meses pasados, 
  como en los días 
  en que Dios me protegía,
  {v}cuando su luz brillaba sobre mi cabeza,
  y su luz me guiaba en las tinieblas!
  {v}Cual era en la madurez de mi vida,
  cuando era amigo de Dios 
  y Éste guardaba mi morada;
{finis:prosaica}
```

Produciría lo siguiente:

``` text
Último discurso de Job. ¹Siguió explicando y dijo:

²“¡Ojalá volviera a ser 
como en los meses pasados, 
como en los días 
en que Dios me protegía,
³cuando su luz brillaba sobre mi cabeza,
y su luz me guiaba en las tinieblas!
⁴Cual era en la madurez de mi vida,
cuando era amigo de Dios 
y Éste guardaba mi morada;
```

## Pendiente por definir

listas ordenadas, listas desordenadas, citas textuales, configuración de libros, inclusion de libros en uno solo (como sucede con la Biblia).
