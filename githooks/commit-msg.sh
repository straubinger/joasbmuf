#!/bin/bash

# Ruta al archivo del mensaje de commit
COMMIT_MSG_FILE=$1

# Lista de prefijos permitidos
PREFIXES="^(caract|correc|docs|estilo|refactor|prueba|tarea)\([a-zA-Z0-9\-_]+\): .+"

# Leer el mensaje de commit
COMMIT_MSG=$(head -n 1 "$COMMIT_MSG_FILE")

# Comprobar si el mensaje de commit cumple con las condiciones
if [[ ${#COMMIT_MSG} -le 70 ]] && echo "$COMMIT_MSG" | grep -qE "$PREFIXES"; then
    exit 0 # Permitir el commit
else
    echo "Error: el mensaje de commit no cumple con las condiciones."
    echo "Debe tener un prefijo válido, un módulo entre paréntesis y no sobrepasar los 70 caracteres."
    echo "Prefijos validos: caract, correc, docs, estilo, refactor, prueba, tarea. Consulte PREFIJOS.md para más información."
    exit 1 # Rechazar el commit
fi
