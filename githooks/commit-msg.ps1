# commit-msg.ps1

# Ruta al archivo del mensaje de commit
$CommitMsgFile = $args[0]

# Lista de prefijos permitidos
$Prefixes = "^(caract|correc|docs|estilo|refactor|prueba|tarea)\([a-zA-Z0-9\-_]+\): .+"

# Leer la primera línea del mensaje de commit
$CommitMsg = (Get-Content $CommitMsgFile)[0]

# Comprobar si el mensaje de commit cumple con las condiciones
if (($CommitMsg.Length -le 70) -and ($CommitMsg -match $Prefixes)) {
    exit 0 # Permitir el commit
} else {
    Write-Host "Error: el mensaje de commit no cumple con las condiciones."
    Write-Host "Debe tener un prefijo válido, un módulo entre paréntesis y no sobrepasar los 70 caracteres."
    Write-Host "Prefijos validos: caract, correc, docs, estilo, refactor, prueba, tarea. Consulte PREFIJOS.md para más información."
    exit 1 # Rechazar el commit
}
